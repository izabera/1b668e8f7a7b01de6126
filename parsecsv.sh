#!/bin/bash
# csv parser with stupid multi-dimensional indexed arrays
# parses csv flies with NL terminated lines
# (fields without leading/trailing whitespace)

# usage: parsecsv csvfile [delimiter]



# support functions:
# create_array arrayname...
# get_element arrayname index...
# set_element val arrayname index...

shopt -s expand_aliases
alias create_array='declare -A'
get_element() { eval "REPLY=\${$1["'${*:2}'"]}"; }
set_element() { eval "$2[\${*:3}]=\$1"; }


delim=${2-,}
if [[ ! -r $1 ]]; then
  echo "can't read $1" >&2
  exit 1
fi

create_array csv
declare -i row=0 col=0 quoted=0 escaped=0 numfields=0
curstring=

# now create a 2d array with all the values
while read -rN1; do
  case $REPLY in

       '"') if [[ -z $curstring ]]; then
              if (( quoted )); then
                read -rN1
                case $REPLY in
                          # field starts with an embedded quote
                     '"') curstring+=$REPLY ;;
 
                          # empty field
                "$delim") set_element "" csv "$row" "$col"
                          curstring= col+=1 quoted=0
                          (( numfields < col )) && numfields=col
                          ;;
 
                          # end of line
                   $'\n') set_element "" csv "$row" "$col"
                          (( numfields < col )) && numfields=col
                          curstring= col=0 row+=1 quoted=0
                          ;;
 
                          # anything else is invalid
                       *) echo error >&2 ;;
 
                esac
 
              else
                # this field will be terminated by a "
                quoted=1
              fi
            else
              # possibly end of field or escaped quote
              read -rN1
              case $REPLY in
                       # escaped quote
                  '"') curstring+='"' ;;
 
                       # end of line  (save the number of fields)
                $'\n') set_element "$curstring" csv "$row" "$col"
                       (( numfields < col )) && numfields=col
                       curstring= row+=1 col=0 quoted=0 ;;
 
                       # end of field
             "$delim") set_element "$curstring" csv "$row" "$col"
                       curstring= col+=1 quoted=0
                       (( numfields < col )) && numfields=col
                       ;;
 
                       # end of file
                   '') set_element "$curstring" csv "$row" "$col" ;;
 
                    *) #invalid
              esac
            fi ;;
 
  "$delim") if (( quoted )); then
              curstring+=$delim
            else
              set_element "$curstring" csv "$row" "$col"
              curstring= col+=1 quoted=0
              (( numfields < col )) && numfields=col
            fi ;;
 
     $'\n') if (( quoted )); then
              curstring+=$REPLY
            else
              set_element "$curstring" csv "$row" "$col"
              (( numfields < col )) && numfields=col
              curstring= row+=1 col=0 quoted=0
            fi ;;
 
         *) curstring+=$REPLY ;;

  esac
done < "$1"




# now it's in the array, we can iterate over it and do whatever we want
# example: let's print it with < > instead of the quotes
for (( i = 0; i < row; i++ )); do
  get_element csv "$i" 0
  printf '<%s>' "$REPLY"
  for (( j = 1; j <= numfields; j++ )); do
    get_element csv "$i" "$j"
    printf ',<%s>' "$REPLY"
  done
  echo
done
